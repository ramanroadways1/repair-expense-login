<?php
require_once '_connect.php';

$bilty_no = escapeString($conn,($_POST['bilty_no']));
$tno = escapeString($conn,($_POST['tno']));

$from_date = date('Y-m-d', strtotime(escapeString($conn,$_POST['duration']), strtotime(date("Y-m-d"))));
$to_date = date("Y-m-d");

if($bilty_no!='')
{
	$where_condition = "bilty_no='$bilty_no'";
	
	$sql = Qry($conn,"SELECT bilty_no,company,lrdate,pod_date,lr_by,billing_type,veh_placer,broker,billing_party,tno,frmstn as from_loc,tostn as to_loc,
	awt,cwt,rate,tamt as amount,billing_branch,bill_no,bill_amount,weight as bill_weight,bill_datetime as bill_timestamp FROM mkt_bilty 
	WHERE $where_condition");
}
else
{
	if($_POST['duration']!='' AND $tno!=''){
		$where_condition = "date BETWEEN '$from_date' AND '$to_date' AND tno='$tno'";
	}
	else if($_POST['duration']!='' AND $tno==''){
		$where_condition = "date BETWEEN '$from_date' AND '$to_date'";
	}
	else if($_POST['duration']=='' AND $tno!=''){
		$where_condition = "tno='$tno'";
	}
	else{
		AlertRightCornerError("Atleast one field is required !");
		exit();
	}
	
	$sql = Qry($conn,"SELECT bilty_no,company,lrdate,pod_date,lr_by,billing_type,veh_placer,broker,billing_party,tno,frmstn as from_loc,tostn as to_loc,
	awt,cwt,rate,tamt as amount,billing_branch,bill_no,bill_amount,weight as bill_weight,bill_datetime as bill_timestamp FROM mkt_bilty 
	WHERE $where_condition ORDER BY id ASC");
}

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	
?>
<!--
<a target="_blank" href="./download_excel.php?report_type=<?php echo $where_condition; ?>"><button type="button" class="btn btn-xs btn-danger"><i class="fa fa-download" aria-hidden="true"></i> Download Excel</button></a>
-->
<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Vehicle_No</th>
			<th>Bilty_No</th>
			<th>Company</th>
			<th>LR_Date</th>
			<th>POD_Date</th>
			<th>LR_By</th>
			<th>Billing_Type</th>
			<th>Vehicle_Placer</th>
			<th>Broker</th>
			<th>Billing_Party</th>
			<th>From</th>
			<th>To</th>
			<th>Act_Wt</th>
			<th>Chrg_Wt</th>
			<th>Rate</th>
			<th>Amount</th>
			<th>Billing_Branch</th>
			<th>Bill_No.</th>
			<th>Bill_Amount</th>
			<th>Bill_Weight</th>
			<th>Bill_Timestamp</th>
		</tr>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

	while($row = fetchArray($sql))
	{
		$lrdate = date('d-m-y', strtotime($row['lrdate']));
		
		if($row['bill_timestamp']!=0){
			$bill_timestamp = date('d-m-y H:i A', strtotime($row['bill_timestamp']));
		}else{
			$bill_timestamp = "<font color='red'>Bill_Pending</font>";
		}
		
		if($row['pod_date']!=0){
			$pod_date = date('d-m-y', strtotime($row['pod_date']));
		}else{
			$pod_date = "<font color='red'>POD_Pending</font>";
		}
		
		echo "<tr>	
			<td>$sn</td>
			<td>$row[tno]</td>
			<td>$row[bilty_no]</td>
			<td>$row[company]</td>
			<td>$lrdate</td>
			<td>$pod_date</td>
			<td>$row[lr_by]</td>
			<td>$row[billing_type]</td>
			<td>$row[veh_placer]</td>
			<td>$row[broker]</td>
			<td>$row[billing_party]</td>
			<td>$row[from_loc]</td>
			<td>$row[to_loc]</td>
			<td>$row[awt]</td>
			<td>$row[cwt]</td>
			<td>$row[rate]</td>
			<td>$row[amount]</td>
			<td>$row[billing_branch]</td>
			<td>$row[bill_no]</td>
			<td>$row[bill_amount]</td>
			<td>$row[bill_weight]</td>
			<td>$bill_timestamp</td>
		</tr>";
		$sn++;
	}
echo "</tbody>
</table>";

closeConnection($conn);
?>
	
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 
