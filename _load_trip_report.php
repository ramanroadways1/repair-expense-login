<?php
require_once '_connect.php';

$from_date = escapeString($conn,($_POST['from_date']));
$to_date = escapeString($conn,($_POST['to_date']));
$tno = escapeString($conn,($_POST['tno']));

$sql = Qry($conn,"SELECT t.trip_no,t.tno,t.branch,t.edit_branch,t.from_station,t.to_station,t.act_wt,t.charge_wt,t.lr_type,t.lrno,t.date,t.end_date,
d.name as driver_name FROM diesel_api.all_trips AS t 
LEFT OUTER JOIN dairy.driver AS d ON d.code = t.driver_code 
WHERE t.date>='$from_date' AND t.end_date<='$to_date' AND t.tno='$tno'");

if(!$sql){
	echo getMySQLError($conn);
	errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
	exit();
}
	
if(numRows($sql)==0)
{
	AlertRightCornerError("No record found !");
	exit();
}
	
?>
<a target="_blank" href="./download_excel.php?report_type=TRIP_REPORT&tno=<?php echo $tno; ?>&from_date=<?php echo $from_date; ?>&to_date=<?php echo $to_date; ?>"><button type="button" class="btn btn-xs btn-danger"><i class="fa fa-download" aria-hidden="true"></i> Download Excel</button></a>
<table id="example" class="table table-bordered table-striped" style="font-size:13px;">
        <thead>
		<tr>
			<th>#</th>
			<th>Trip_No</th>
			<th>Driver</th>
			<th>Branch</th>
			<th>Edit_Branch</th>
			<th>From</th>
			<th>To</th>
			<th>Act_Wt</th>
			<th>Chrg_Wt</th>
			<th>LR_Type</th>
			<th>LR_No</th>
			<th>Trip_Date</th>
			<th>End_Date</th>
		</thead>
    <tbody id=""> 
	
<?php
$sn=1;

	while($row = fetchArray($sql))
	{
		$trip_date = date('d-m-y', strtotime($row['date']));
		
		if($row['end_date']!=0){
			$end_date = date('d-m-y', strtotime($row['end_date']));
		}else{
			$end_date = "<font color='green'>Running</font>";
		}
		
		echo "<tr>	
			<td>$sn</td>
			<td>$row[trip_no]</td>
			<td>$row[driver_name]</td>
			<td>$row[branch]</td>
			<td>$row[edit_branch]</td>
			<td>$row[from_station]</td>
			<td>$row[to_station]</td>
			<td>$row[act_wt]</td>
			<td>$row[charge_wt]</td>
			<td>$row[lr_type]</td>
			<td>$row[lrno]</td>
			<td>$trip_date</td>
			<td>$end_date</td>
		</tr>";
		$sn++;
	} 
echo "</tbody>
</table>";

closeConnection($conn);
?>
	
<script> 
$("#loadicon").fadeOut('slow');
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script> 
