<?php
require_once("./_connect.php");
  
$timestamp = date("Y-m-d H:i:s");
$date = date("Y-m-d");

$trip_id = escapeString($conn,($_POST['trip_id']));
$amount = escapeString($conn,($_POST['amount']));
$exp_id = escapeString($conn,($_POST['exp_id']));
$narration = escapeString($conn,($_POST['narration']));
$slip_no = escapeString($conn,($_POST['slip_no']));

if($trip_id != $_SESSION['exp_trip_id'])
{
	AlertErrorTopRight("Trip not verified !");
	exit();
}

$get_trip = Qry($conn,"SELECT branch,trip_no,driver_code,tno,lr_type FROM dairy.trip WHERE id='$trip_id'");

if(!$get_trip){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_trip)==0)
{
	AlertErrorTopRight("Trip not found !");
	exit();
}

$row_trip = fetchArray($get_trip);

$tno = $row_trip['tno'];
$trip_no = $row_trip['trip_no'];
$branch = $row_trip['branch'];
$driver_code = $row_trip['driver_code'];

if(empty($driver_code))
{
	AlertErrorTopRight("Driver not found !");
	exit();
}

$get_expense = Qry($conn,"SELECT exp_code,name,visible_to_supervisor,lock_on_empty FROM dairy.exp_head WHERE id='$exp_id'");

if(!$get_expense){
	AlertErrorTopRight("Error while processing request !");
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	exit();
}

if(numRows($get_expense)==0)
{
	AlertErrorTopRight("Expense not found !");
	exit();
}

$row_exp = fetchArray($get_expense);

$exp_name = $row_exp['name'];
$exp_code = $row_exp['exp_code'];

$check_scripts = Qry($conn,"SELECT id FROM dairy.running_scripts WHERE file_name!='LOAD_API_TRANS'");

if(!$check_scripts){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($check_scripts)>0)
{
	AlertErrorTopRight("Please try after some time !");
	exit();
}

$hisab_cache = Qry($conn,"SELECT id FROM dairy.hisab_cache WHERE tno='$tno'");

if(!$hisab_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($hisab_cache)>0)
{
	AlertErrorTopRight("Vehicle hisab in process !");
	exit();
}

$trip_cache = Qry($conn,"SELECT id FROM dairy.trip_cache WHERE tno='$tno'");

if(!$trip_cache){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	AlertErrorTopRight("Error while processing request !");
	exit();
}

if(numRows($trip_cache)>0)
{
	AlertErrorTopRight("Please try after some time !");
	exit();
}

if(count($_FILES['bill']['name'])>0)
{
	$valid_types = array("image/jpg", "image/jpeg", "image/bmp", "image/gif","image/png");
	
	foreach($_FILES['bill']['type'] as $file_types)
	{
		if(!in_array($file_types, $valid_types))
		{
			AlertErrorTopRight("Only Image Upload Allowed !");
			exit();
		}
    }
}
else
{
	AlertErrorTopRight("Please Upload Invoice Copy !");
	exit();
}

StartCommit($conn);
$flag = true;	

$trans_id_Qry = GetTxnId_eDiary($conn,"EXP");

if(!$trans_id_Qry || $trans_id_Qry=="" || $trans_id_Qry=="0"){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$trans_id = $trans_id_Qry;	

for($i=0; $i<count($_FILES['bill']['name']);$i++) 
{
	$sourcePath = $_FILES['bill']['tmp_name'][$i];
			
	$fix_name = date('dmYHis').mt_rand();
		
	$targetPath = "bill_repair/".$fix_name.".".pathinfo($_FILES['bill']['name'][$i],PATHINFO_EXTENSION);
		
	ImageUpload(800,800,$sourcePath);
		
	if(move_uploaded_file($sourcePath, $targetPath)) 
	{
		$files_uploads[] = $targetPath;
	}
	else
	{
		$flag = false;
		errorLog("File upload failed.",$conn,$page_name,__LINE__);
    }
		
	$repair_bills = implode(',',$files_uploads);
}
	
$insert_exp_diary = Qry($conn,"INSERT INTO dairy.trip_exp(trip_id,trans_id,tno,exp_name,exp_code,amount,date,narration,branch,timestamp) VALUES 
('$trip_id','$trans_id','$tno','$exp_name','$exp_code','$amount','$date','$narration','$branch','$timestamp')");

if(!$insert_exp_diary){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$insert_exp_file = Qry($conn,"INSERT INTO dairy.repair_exp_file (trans_id,slip_no,files,timestamp) VALUES ('$trans_id','$slip_no','$repair_bills',
'$timestamp')");

if(!$insert_exp_file){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$update_trip = Qry($conn,"UPDATE dairy.trip SET expense=expense+'$amount' WHERE id='$trip_id'");

if(!$update_trip){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$select_amount = Qry($conn,"SELECT id,amount_hold FROM dairy.driver_up WHERE down=0 AND code='$driver_code' ORDER BY id DESC LIMIT 1");

if(!$select_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

$row_amount = fetchArray($select_amount);

$driver_bal_id = $row_amount['id'];
$hold_amt = $row_amount['amount_hold']-$amount;

$insert_book = Qry($conn,"INSERT INTO dairy.driver_book (driver_code,tno,trip_id,trip_no,trans_id,desct,debit,balance,date,branch,timestamp) VALUES 
('$driver_code','$tno','$trip_id','$trip_no','$trans_id','EXP-$exp_name','$amount','$hold_amt','$date','$branch','$timestamp')");

if(!$insert_book){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}	

$update_hold_amount = Qry($conn,"UPDATE dairy.driver_up SET amount_hold=amount_hold-'$amount' WHERE id='$driver_bal_id'");

if(!$update_hold_amount){
	$flag = false;
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
}

if($flag)
{
	MySQLCommit($conn);
	closeConnection($conn);
	
	AlertRightCornerSuccess("Expense Added Successfully !");
	
	echo "<script>
			$('#Form2')[0].reset();
			$('#exp_submit').attr('disabled',false);
			$('#exp_modal_close_btn')[0].click();
		</script>";
	exit();
}
else
{
	MySQLRollBack($conn);
	closeConnection($conn);
	AlertErrorTopRight("Error while processing request !");
	exit();
}	
?>