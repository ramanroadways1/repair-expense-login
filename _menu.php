<?php
require_once("_connect.php");

$page_name1 = basename($_SERVER['PHP_SELF']);
?>

<link href="google_font.css" rel="stylesheet">

<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}

@media (min-width: 768px) {
  .modal-xl-mini {
    width: 75%;
   max-width:100%;
  }
}

.modal { overflow: auto !important; } 

.selectpicker { width:auto; font-size: 12px !important;}

::-webkit-scrollbar{
    width:4px;
    height:4px;
}
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}

table{
	font-family: 'Verdana', sans-serif !important;
	font-size:11px !important;
}

table>thead>tr>th{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12px !important;
}

.ui-autocomplete { z-index:2147483647; font-size:13px !important;}
</style>

<style type="text/css">
label{
	font-family: 'Open Sans', sans-serif !important;
	font-size:12.5px !important;
}
input[type='date'] { font-size: 12.5px !important;}
input[type='text'] { font-size: 12.5px !important;}
select { font-size: 12.5px !important; }
textarea { font-size: 12.5px !important; }
</style>

<div id="loadicon" style="position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1; cursor: wait">
	<center><img style="margin-top:140px" src="loading_truck1.gif" /><br><br><span style="letter-spacing:1px;font-weight:bold;font-size:14px">कृप्या प्रतीक्षा करे ..</span></center>
</div>

<!--<div class="se-pre-con"><span class="text11">RAMAN ROADWAYS</span></div>-->
 
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

	<header class="main-header"> 
        <a href="./" class="logo" style="background:#FFF">
			<span class="logo-mini"><img src="../_logo/logo_raman_small.png" style="width:100%;height:50px" class="" /></span>
			<span class="logo-lg" id="logo_desktop"><img src="../_logo/logo_raman_main.png" style="margin-top:5px;width:100%;height:40px" class="img-responsive" /></span>
			<span class="logo-lg" id="logo_mobile"><center><img src="../_logo/logo_raman_main.png" style="margin-top:5px;width:50%;height:40px" class="img-responsive" /></center></span>
        </a>
    
	<nav class="navbar navbar-static-top" role="navigation">
		  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		  </a>
		  <div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
			<li class="user-menu">
				<a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
				  <img src="avtar.png" class="user-image" alt="User Image">
				  <span class="hidden-xs"><?php echo $_SESSION['d_repair_exp']; ?></span>
					&nbsp; &nbsp; <button onclick="LogoutFunc1()" type="button" 
					class="btn btn-xs btn-danger"><i class="fa fa-power-off"></i> Logout</button></a>
				</a>
			  </li>
			</ul>
		  </div>

    </nav>
    </header>
	
<script>
function LogoutFunc1()
{
	Swal.fire({
	  title: 'Are you sure ??',
	  // text: "",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, i Confirm !'
	}).then((result) => {
	  if (result.isConfirmed) {
		window.location.href='./logout.php';
	  }
	})
}

function CallUrl(url)
{
	if(url!='')
	{
		window.location.href=url;
	}
}
</script>	

<style>
.sidebar-menu>li>a{
	cursor:pointer;
}

.treeview>ul>li>a{
	font-family:Verdana !important;
	font-size:11px !important;
	cursor:pointer;
}

.fa-circle-o{
	font-size:10px !important
}
</style>	
     
<aside class="main-sidebar">
    <section class="sidebar">
		<div class="user-panel"></div>
	  
          <ul style="font-size:13px !important;" class="sidebar-menu" data-widget="tree">
            
			<li class="<?php if($page_name1=="index_main.php") {echo "active";} ?>">
              <a onclick="CallUrl('./index_main.php')"><i class="fa fa-dashboard"></i> <span>Dashboard</span> </a>
			</li>
			
			<li class="<?php if($page_name1=="trip_running.php") {echo "active";} ?>">
              <a onclick="CallUrl('./trip_running.php')"><i class="fa fa-retweet"></i> <span>Running Trips</span> </a>
			</li>
			
			<li class="<?php if($page_name1=="trip_completed.php") {echo "active";} ?>">
              <a onclick="CallUrl('./trip_completed.php')"><i class="fa fa-search-minus"></i> <span>Completed Trips</span> </a>
			</li>
			
			<li>
				<a onclick="LogoutFunc1();"><i class="fa fa-power-off"></i> <span>Logout</span> </a>
			</li>
		</ul>
        </section>
    </aside>