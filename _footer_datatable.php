<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date("Y"); ?> RAMAN ROADWAYS PVT LTD.</strong> All rights reserved.
</footer>
 
</div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="../admin_lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../admin_lte/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="../admin_lte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../admin_lte/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="../admin_lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../admin_lte/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../admin_lte/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../admin_lte/dist/js/demo.js"></script>
    <!-- page script -->
    <script>
      $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
  </body>
</html>

<script type="text/javascript">
  $(window).load(function() {
    $("#loadicon").fadeOut("slow");;
  });
</script>
